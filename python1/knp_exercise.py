#coding: utf-8
from pyknp import KNP
import sys

knp = KNP()
data = ""
noun = []
count = 0
for line in iter(sys.stdin.readline, ""):
    data += line
    if line.strip() == "EOS":
        result = knp.result(data)
        for bnst in result.bnst_list(): # 各文節についてのforループ
            for mrph in bnst.mrph_list(): # 文節内に含まれる形態素をforループでチェック
                if mrph.hinsi == "名詞":
                    # 接頭辞があったので、文節まるごと出力
                    noun.append(mrph.midasi)
                    count += 1
            if count >=2:
                print("".join(noun))
            noun = []
            count = 0
            data = ""
                                                                                                                                                                                

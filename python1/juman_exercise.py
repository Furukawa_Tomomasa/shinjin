#coding: utf-8
from pyknp import Juman
import sys
 
juman = Juman()

def exercise_62():
    print("exercise_62")
    data = ""
    for line in iter(sys.stdin.readline, ""): # 入力文を1行ずつ読む
        data += line
        if line.strip() == "EOS": # 1文が終わったら解析
            result = juman.result(data)
            print(",".join(mrph.midasi for mrph in result.mrph_list()))
            data = ""
    print("------------------------------")
def exercise_63():
    print("exercise_63")
    data = ""
    for line in iter(sys.stdin.readline, ""): # 入力文を1行ずつ読む
        data += line
        if line.strip() == "EOS": # 1文が終わったら解析
            result = juman.result(data)
            for mrph in result.mrph_list():
                if (mrph.hinsi == "動詞"):
                    print(mrph.genkei,mrph.hinsi)
            data = ""
    print("------------------------------")
def exercise_64():
    print("exercise_64")
    data = ""
    dict = {}
    for line in iter(sys.stdin.readline, ""): # 入力文を1行ずつ読む
        data += line
        if line.strip() == "EOS": # 1文が終わったら解析
            result = juman.result(data)
            for mrph in result.mrph_list():
                if mrph.genkei not in dict:
                    dict[mrph.genkei] = 1
                else:
                    dict[mrph.genkei] += 1
            data = ""
    for k,v in sorted(dict.items(), key=lambda x:x[1],reverse=True):
        print(k,v)
    print("------------------------------")
def exercise_65():
    print("exercise_65")
    data = ""
    words_count = 1
    predicates_count = 1
    for line in iter(sys.stdin.readline, ""): # 入力文を1行ずつ読む
        data += line
        if line.strip() == "EOS": # 1文が終わったら解析
            result = juman.result(data)
            for mrph in result.mrph_list():
                words_count += 1
                if (mrph.hinsi == "動詞" or mrph.hinsi == "形容詞" or mrph.hinsi == "形容動詞"):
                    predicates_count += 1
            data = ""
    print("{0:.3f}".format(predicates_count / words_count * 100))
    print("------------------------------")
def exercise_66():
    print("exercise_66")
    data = ""
    words_count = 0
    words_bunrui = []
    words_midasi = []
    words_repname = []
    for line in iter(sys.stdin.readline, ""): # 入力文を1行ずつ読む
        data += line
        if line.strip() == "EOS": # 1文が終わったら解析
            result = juman.result(data)
            for mrph in result.mrph_list():
                words_bunrui.append(mrph.bunrui)
                words_midasi.append(mrph.midasi)
                words_repname.append(mrph.repname)
                words_count += 1
            for i in range(0,words_count-1):
                if words_bunrui[i] == "サ変名詞" and ("する" in words_repname[i+1] or "できる" in words_repname[i+1]):
                    print(words_midasi[i] + words_midasi[i+1])
            words_bunrui = []
            words_midasi = []
            words_repname = []
            words_count = 0
            data = ""
    print("------------------------------")
def exercise_67():
    print("exercise_67")
    data = ""
    words_count = 0
    words_hinsi = []
    words_midasi = []
    for line in iter(sys.stdin.readline, ""): # 入力文を1行ずつ読む
        data += line
        if line.strip() == "EOS": # 1文が終わったら解析
            result = juman.result(data)
            for mrph in result.mrph_list():
                words_hinsi.append(mrph.hinsi)
                words_midasi.append(mrph.midasi)
                words_count += 1
            for i in range(1,words_count-2):
                if words_midasi[i] == "の" and words_hinsi[i-1] == "名詞" and words_hinsi[i+1] == "名詞":
                    print(words_midasi[i-1] + words_midasi[i] + words_midasi[i+1])
            words_hinsi = []
            words_midasi = []
            words_count = 0
            data = ""
    print("------------------------------")

#exercise_62()
#exercise_63()
#exercise_64()
#exercise_65()
#exercise_66()        
#exercise_67()

import sys

imput_numbers = [1,2,6,3,5,10,9,13]

def homework1():
    imputs = []
    numbers = []
    args = sys.argv
    for a in args[1]:
        imputs.append(a)
    for number in imputs:
        numbers.append(number)
    numbers.sort(reverse=True)
    return (numbers[0])

#数字の入った配列を受け取り、その中の最大の値とその添字(インデックス)を返す関数を実装せよ
def homework2(imp):
    dic_num = {}
    for i in range(len(imput_numbers)):
        dic_num[i] = imput_numbers[i]
    sorted(dic_num.items(), key=lambda x:x[1], reverse=True)
    return ((sorted(dic_num.items(), key=lambda x:x[1], reverse=True))[0])


def homework3(imp):
    change_num = []
    for i in range(len(imp)):
        if i == 0:
            change_num.append(imp[i])
        else:
            change_num.append(0)
    return change_num

def imput_number():
    args = sys.argv
    n = int(args[1])
    return n
def homework4_1(imp):
    v = 0
    if (imp == 1 or imp == 2):
        v = 1
        return v
    else:
        v += homework4_1(imp-2) + homework4_1(imp-1)
        return v
def homework4_2(imp):
    v_list = []
    for i in range(0, imp):
        if (i ==  0 or i == 1):
            v_list.append(1)
        else:
            v_list.append(v_list[i-1] + v_list[i-2])
    return v_list

#homework4_3()
#再起呼び出しを用いると遡っていって順々に戻っていくので、用いない方と比べて計算量が多い

#homework5
def quicksort(seq):
    left = []
    right = []
    if len(seq) > 1:
        standard = seq[0]
        for i in range(1,len(seq)):
            if seq[i] < standard:
                left.append(seq[i])
            else:
                right.append(seq[i])
        left = quicksort(left)
        right = quicksort(right)
        return left + [standard] + right
    else:
        return seq

#print(homework1())
#print(homework2(imput_numbers))
#print(homework3(imput_numbers))
#imput_n = imput_number()
#print(homework4_1(imput_n))
#print(homework4_2(imput_n))
#print(quicksort(imput_numbers))

#coding :utf-8
import re
total_count = 0
#解析する文に適しているかどうかを判定
def is_valid(line):
    if re.match(r'^<PAGE\s+URL=', line) or re.match(r'^</PAGE>', line):
        return False
    else:
        return True
#bigramとunigramでの回数をカウントし、辞書に格納する
def count_frequency(words,pattern_count):
    global total_count
    for i in range(len(words)-1):
        if words[i] + " " + words[i+1] in pattern_count:
            pattern_count[words[i] + " " + words[i+1]] += 1
        else:
            pattern_count[words[i] + " " + words[i+1]] = 1
    for i in range(len(words)):
        total_count += 1
        if words[i] in pattern_count:
            pattern_count[words[i]] += 1
        else:
            pattern_count[words[i]] = 1
#doc0000000000.txtの各単語の頻度をカウント
def bigram_language_model(pattern_count):
    words = []
    with open("doc0000000000.txt") as my_file:
        for line in my_file:
            if is_valid(line):
                words = line.lower().strip().split()
                count_frequency(words,pattern_count)
#辞書から指定された単語の頻度を引っ張ってくる
def freq(pattern):
    return pattern_frequency[pattern]
#任意のsentenceの確率を計算
def calc_probability_sentence(sentence):
    words = []
    p = 1
    #任意の文の単語数を数えられなかったのでrange(5)で対処しました
    """for line in sentence:
        words = re.split(r'\s|\,|\.|\(|\)', line.lower())
        print(words)"""
    for i in range(4):
        p *= freq(sentence.split()[i].lower())/total_count * freq(sentence.split()[i].lower() + " " + sentence.split()[i+1].lower()) / freq(sentence.split()[i].lower())
    return p
sentence = "The man is in the house."
pattern_frequency = {}
bigram_language_model(pattern_frequency)
prob_sentence = calc_probability_sentence(sentence)
print(prob_sentence)
                                                                                                                                                                                                                            

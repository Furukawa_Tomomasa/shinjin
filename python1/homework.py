import math
def homework1():
    print("homework1")
    for i in range(30):
        print(str(i+1),end=" ")
    print("")
    print("-----------------")
def homework2():
    print("homework2")
    for i in range(2,31,2):
        print(str(i),end=" ")
    print("")
    print("-----------------")
def homework3():
    sum = 0
    i = 0
    print("homework3")
    while (i < 30):
        sum += (i+1)
        i += 1
    print(sum)
    print("-----------------")
def homework4():
    sum = 1
    print("homework4")
    for i in range(10):
        sum *= (i+1)
    print(sum)
    print("-----------------")
def homework5():
    print("homework5")
    for i in range(9):
        for j in range(9):
            print(str((i+1) * (j+1)),end="\t")
        print("")
    print("-----------------")
def homework6():
    print("homework6")
    for i in range(30):
        if (i+1) % 3 == 0:
            print("Fizz",end="")
            if (i+1) % 5 == 0:
                print("Buzz",end="")
            print("",end="\t")
        elif (i+1) % 5 == 0:
            print("Buzz",end="\t")
        else:
            print(str(i+1),end="\t")
    print("")
    print("-----------------")
def homework7():
    #(i+1)の1/2乗までの約数の個数
    times = 0
    print("homework7")
    #1が素数でないことからiの範囲を1からにして、1000まで繰り返すためi+1で考慮している
    for i in range(1,1000):
        for j in range(1,(int(math.sqrt(i+1)))+1):
            if ((i+1) % j) == 0: #jも0を考慮しないためにj+1にしている
                times += 1
        if times == 1:
            print(str(i+1),end="\t")
        times = 0
homework1()
homework2()
homework3()
homework4()
homework5()
homework6()
homework7()

def exercise8_1():
    foods = ["egg", "spam", "spam", "bacon", "bacon", "meat"]
    counts = {}
    for i in foods:
        if not (i in counts):
            counts[i] = 1
        else:
            counts[i] += 1
    for word,cnt in sorted(counts.items(), key=lambda x:x[1], reverse=True):
        print(word, cnt)
    
def exercise8_2():
    alpha = []
    con_alpha = {}
    rein_alpha = {}
    team = "lions"
    con_team = []
    rein_team = []
    for i in range(ord("a"), ord("z")+1):
        alpha.append(chr(i))
    for l in range(len(alpha)):
        con_alpha[alpha[l]] = alpha[l-3]
    for k,v in con_alpha.items():
        rein_alpha[v] = k
    for char in team:
        con_team.append(con_alpha[char])
    for char in con_team:
        rein_team.append(rein_alpha[char])
    print(team)
    print(con_team)
    print("".join(rein_team))






exercise8_1()
exercise8_2()

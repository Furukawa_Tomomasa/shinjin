def exercise7_1():
    teams = ["Yakult", "Swallows"]
    for i in teams:
        print(i[0], end="")
def exercise7_2():
    team = "yakultswallows"
    print("Strings have", end=" ")
    if team.isalnum():
        print("alnum",end=" ")
    if team.lower():
        print("lower",end=" ")
    if team.isspace():
        print("space",end=" ")
    print(".")
def exercise7_3():
    str = "stressed"
    print(str[::-1])
def exercise7_4():
    str = "パタトクカシーー"
    print("".join(str[::2]))
def exercise7_5():
    str1 = "パトカー"
    str2 = "タクシー"
    c =[]
    for a,b in zip(str1,str2):
        c.append(a+b)
    print("".join(c))
def exercise7_6():
    sentence = "Hi He Lied Because Boron Could Not Oxidize Fluorine. New Nations Might Also Sign Peace Security Clause. Arthur King Can."
    words = sentence.split(" ")
    characters = []
    #1, 5, 6, 7, 8, 9, 15, 16, 19番目の単語は先頭の1文字，それ以外の単語は先頭に2文字を取り出し，取り出した文字列を結合せよ
    for i in range(len(words)):
        word = words[i]
        if ((i+1) == 1) or ((i+1) == 5) or ((i+1) == 6) or ((i+1) == 7) or ((i+1) == 8) or ((i+1) == 9) or ((i+1) == 15) or ((i+1) == 16) or ((i+1) == 19):
        characters.append(word[0])
        else:
        characters.append(word[1])
    print("".join(characters))
    
    
    
    
exercise7_1()
exercise7_2()
exercise7_3()
exercise7_4()
exercise7_5()
exercise7_6()
